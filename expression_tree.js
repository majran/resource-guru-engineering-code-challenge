const assert = require("assert");

/**
 * For numbers we use Expression class to access result and toString methods
 */
class Expression {
  /**
   * @class
   * @param {Number} value
   */
  constructor(value) {
    this.value = value;
  }

  get value() {
    return this._value;
  }

  set value(newValue) {
    if (isNaN(newValue)) {
      throw new Error(`Invalid value, Expression value should be a number`);
    } else {
      this._value = newValue;
    }
  }

  result() {
    return this.value;
  }

  toString() {
    return this.value.toString();
  }
}

/**
 * An abstract class for Operations
 */
class Operation {
  /**
   * @class
   * @param {Expression, Operation} left
   * @param {Expression, Operation} right
   */
  constructor(left, right) {
    this.left = left;
    this.right = right;
  }
  get left() {
    return this._left;
  }

  set left(newLeft) {
    if (!(newLeft instanceof Expression || newLeft instanceof Operation)) {
      throw new Error(
        `Invalid left, Left should be an Expression or an Operation`
      );
    } else {
      this._left = newLeft;
    }
  }

  get right() {
    return this._right;
  }

  set right(newRight) {
    if (!(newRight instanceof Expression || newRight instanceof Operation)) {
      throw new Error(
        `Invalid right, right should be an Expression or an Operation`
      );
    } else {
      this._right = newRight;
    }
  }

  /**
   * @abstract
   */
  result() {
    throw new Error(`Not implemented`);
  }

  /**
   * @abstract
   */
  toString() {
    throw new Error(`Not implemented`);
  }
}

/**
 * Add Operation
 *
 * @mixes Operation
 */
class Add extends Operation {
  result() {
    return this.left.result() + this.right.result();
  }

  toString() {
    return `(${this.left.toString()} + ${this.right.toString()})`;
  }
}

/**
 * Subtract Operation
 *
 * @mixes Operation
 */
class Subtract extends Operation {
  result() {
    return this.left.result() - this.right.result();
  }

  toString() {
    return `(${this.left.toString()} - ${this.right.toString()})`;
  }
}

/**
 * Multiply Operation
 *
 * @mixes Operation
 */
class Multiply extends Operation {
  result() {
    return this.left.result() * this.right.result();
  }

  toString() {
    return `(${this.left.toString()} x ${this.right.toString()})`;
  }
}

/**
 * Divide Operation
 *
 * @mixes Operation
 */
class Divide extends Operation {
  get right() {
    return this._right;
  }

  set right(newRight) {
    if (newRight.result() == 0) {
      throw new Error(`Invalid operation, Devided by zero`);
    } else {
      this._right = newRight;
    }
  }
  result() {
    return this.left.result() / this.right.result();
  }

  toString() {
    return `(${this.left.toString()} ÷ ${this.right.toString()})`;
  }
}

// Check devide by zero
assert.throws(() => {
  new Divide(new Expression(2), new Add(new Expression(1), new Expression(-1)));
}, new Error(`Invalid operation, Devided by zero`));

// Check invalid left expression
assert.throws(() => {
  new Add(1, new Expression(1));
}, new Error(`Invalid left, Left should be an Expression or an Operation`));

const tree = new Divide(
  new Add(
    new Expression(7),
    new Multiply(
      new Subtract(new Expression(3), new Expression(2)),
      new Expression(5)
    )
  ),
  new Expression(6)
);

assert.strictEqual("((7 + ((3 - 2) x 5)) ÷ 6)", tree.toString());
assert.strictEqual(2, tree.result());
