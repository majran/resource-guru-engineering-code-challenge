const assert = require("assert");

/**
 * flattening_an_array is a recursion function that flattern an arbitrarily nested array of values. 
 * Values can be anything like numbers, string and even object except array)
 *
 *  @param {*} array 
 */
const flattening_an_array = (array) => {
// If input format is an array we have to flatten all items inside this array
  if (Array.isArray(array)) {
    const result = array.reduce((object, item) => {
      object = object.concat(flattening_an_array(item)); // apply flattening_an_array for all items inside the array
      return object;
    }, []); // This empty array is the object input that is used in reduce
    return result;
  } else {
    // Here we return an array because it will concat to another array in the future
    return [array];
  }
};

assert.deepEqual(flattening_an_array([1, [2, [3]], 4]), [1, 2, 3, 4]);

assert.deepEqual(
  flattening_an_array(["a", [2, 3, [["b", { key: "value" }], 1]]]),
  ["a", 2, 3, "b", { key: "value" }, 1]
);
