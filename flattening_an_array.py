
def flattening_an_array(array):
    if type(array) is list:
        result = []
        for item in array:
            result+=flattening_an_array(item)
        return result
    else:
        return [array]

assert flattening_an_array([ 1, [ 2, [ 3 ] ], 4 ]) == [1, 2, 3, 4]